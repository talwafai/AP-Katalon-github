<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_Show All Views</name>
   <tag></tag>
   <elementGuidId>c31b0307-d21b-4707-b977-538a2f562e22</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='View3 assignments'])[1]/following::li[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>body > app-root > main > loader > div > top-bar > div > div.hamburger-menu.left.mright10 > mdl-popover > ul > li:nth-child(1)</value>
      </entry>
   </selectorCollection>
   <selectorMethod>CSS</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>67a046ef-1d84-4b43-ae78-4e8039808b92</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>top-bar-view-setting-cmd ng-star-inserted</value>
      <webElementGuid>b99c6e0f-12d2-456f-acd4-095c6dd600f3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Show All Views </value>
      <webElementGuid>3d4f1295-5d94-4715-b9e0-33dd757de14b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;playbook-theme multiple-products&quot;]/app-root[1]/main[@class=&quot;ng-star-inserted&quot;]/loader[1]/div[1]/top-bar[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;top-bar-container fixed&quot;]/div[@class=&quot;hamburger-menu left mright10&quot;]/mdl-popover[@class=&quot;view-settings-popover mdl-shadow--2dp mdl-popover is-visible&quot;]/ul[@class=&quot;view-setting-command ng-star-inserted&quot;]/li[@class=&quot;top-bar-view-setting-cmd ng-star-inserted&quot;]</value>
      <webElementGuid>181d3269-2ef4-4287-9595-996e282b12e3</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='View3 assignments'])[1]/following::li[1]</value>
      <webElementGuid>c476468d-13e1-49bb-9b53-a03c731a51d4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Section2'])[1]/following::li[2]</value>
      <webElementGuid>ca11a0e5-9e8e-406d-9df5-c7423e2b2001</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Save View Settings'])[1]/preceding::li[1]</value>
      <webElementGuid>3a49aca8-efd0-403f-8381-6f8eff78be93</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Save View As'])[1]/preceding::li[2]</value>
      <webElementGuid>804d1cfc-e1fb-482d-b806-928642a111b3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Show All Views']/parent::*</value>
      <webElementGuid>aef752e0-870b-4722-92b2-1fe2debf5811</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//mdl-popover/ul/li</value>
      <webElementGuid>3c611874-a200-466f-a079-20b612cc3285</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[(text() = ' Show All Views ' or . = ' Show All Views ')]</value>
      <webElementGuid>f64a56ef-df8d-4b7a-88c5-6dbbccc70af8</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
