<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_Edit Team Settings</name>
   <tag></tag>
   <elementGuidId>01baa223-2777-4a2d-a5f1-35cb82ff12ae</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='-'])[1]/following::li[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>01f9a26e-d2c7-4f6b-9c4d-b71aa8386109</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>top-bar-view-setting-cmd ng-star-inserted</value>
      <webElementGuid>e50004e1-09cd-4e29-ad2f-47564d39c51f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Edit Team Settings </value>
      <webElementGuid>89cddd21-3a8a-4865-bbf1-be5fed3cb1fe</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;playbook-theme multiple-products&quot;]/app-root[1]/main[@class=&quot;ng-star-inserted&quot;]/loader[1]/div[1]/top-bar[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;top-bar-container fixed&quot;]/div[@class=&quot;hamburger-menu left mright10&quot;]/mdl-popover[@class=&quot;view-settings-popover mdl-shadow--2dp mdl-popover is-visible&quot;]/ul[@class=&quot;view-setting-command ng-star-inserted&quot;]/li[@class=&quot;top-bar-view-setting-cmd ng-star-inserted&quot;]</value>
      <webElementGuid>f1d08ddf-3e5a-4db5-91cb-7e56a8b366e5</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='-'])[1]/following::li[1]</value>
      <webElementGuid>5429f4cd-cda0-4093-a5e7-756f7ec92bf6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Copy to clipboard'])[1]/following::li[2]</value>
      <webElementGuid>e9f36957-227c-470c-a0a4-935019db56d6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='-'])[2]/preceding::li[1]</value>
      <webElementGuid>fe78c5c3-0a71-4797-850f-5dec15fb982e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Hide Navigation Bar'])[1]/preceding::li[2]</value>
      <webElementGuid>bc97fd8d-75ff-40c9-8406-cd5b685f89bc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Edit Team Settings']/parent::*</value>
      <webElementGuid>ec5e0d69-ee25-470a-a619-41b4b5dafa73</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[8]</value>
      <webElementGuid>cb681871-0952-46aa-acaa-1caca02d3b4a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[(text() = ' Edit Team Settings ' or . = ' Edit Team Settings ')]</value>
      <webElementGuid>bc57ab5c-15ee-4979-8cc2-667f79207db6</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
