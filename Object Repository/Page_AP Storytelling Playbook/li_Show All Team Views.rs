<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_Show All Team Views</name>
   <tag></tag>
   <elementGuidId>689e16cc-834a-4ea1-95a5-440e9e6d66a8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Section Title'])[1]/following::li[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>li.top-bar-view-setting-cmd.ng-star-inserted</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
      <webElementGuid>8c7eddd3-c5ce-479e-a0ce-3a6b6a177fc4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>top-bar-view-setting-cmd ng-star-inserted</value>
      <webElementGuid>3e769054-167a-4d14-96bb-1398517b9733</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Show All Team Views </value>
      <webElementGuid>b946c5ad-e050-406b-9c30-fcdbedd050f4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;playbook-theme multiple-products&quot;]/app-root[1]/main[@class=&quot;ng-star-inserted&quot;]/loader[1]/div[1]/top-bar[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;top-bar-container fixed team-settings&quot;]/div[@class=&quot;hamburger-menu left mright10&quot;]/mdl-popover[@class=&quot;view-settings-popover mdl-shadow--2dp mdl-popover is-visible&quot;]/ul[@class=&quot;view-setting-command ng-star-inserted&quot;]/li[@class=&quot;top-bar-view-setting-cmd ng-star-inserted&quot;]</value>
      <webElementGuid>092564e8-dc22-4032-bebf-b0d6b697647f</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Section Title'])[1]/following::li[1]</value>
      <webElementGuid>2007cecc-fb53-4b4c-92ec-ccc766f65f52</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='DEFAULT'])[1]/following::li[1]</value>
      <webElementGuid>adb9b28f-6e47-4258-b223-caffcabc2427</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Save Team View Settings'])[1]/preceding::li[1]</value>
      <webElementGuid>e8833a03-876e-45da-a188-6e254b0dad57</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Save Team View As'])[1]/preceding::li[2]</value>
      <webElementGuid>2cdf9f86-08a6-4ddd-b799-ac773616bf1d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Show All Team Views']/parent::*</value>
      <webElementGuid>9343be33-1015-446e-a8e4-3240858828a3</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li</value>
      <webElementGuid>02e97545-8432-46d4-8e00-e69dfbc31067</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//li[(text() = ' Show All Team Views ' or . = ' Show All Team Views ')]</value>
      <webElementGuid>d752ea8c-92a2-476e-a90b-196513276426</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Section2'])[1]/following::li[1]</value>
      <webElementGuid>0bf175c5-ae01-4f9a-a8f5-5d1649b740d0</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
