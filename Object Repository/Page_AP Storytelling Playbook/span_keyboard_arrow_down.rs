<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_keyboard_arrow_down</name>
   <tag></tag>
   <elementGuidId>4647e7bf-130b-4ae9-beab-14cd8c2f69b5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>span.mdl-select__toggle.material-icons</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Edition Names'])[1]/following::span[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
      <webElementGuid>f42ee426-1494-410a-ad9c-97ea0b1c5ab7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>mdl-select__toggle material-icons</value>
      <webElementGuid>fc5a082a-659d-41c4-8d79-7f9ee156d1d5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> keyboard_arrow_down </value>
      <webElementGuid>70a36667-0ec2-4681-a470-759be95946d5</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;playbook-theme multiple-products&quot;]/app-root[1]/main[@class=&quot;ng-star-inserted&quot;]/loader[1]/div[1]/main-content[@class=&quot;otouch&quot;]/div[@class=&quot;main-content sidemenu-shrinked&quot;]/manage-publication-channels[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;data-container-wrapper&quot;]/loader[1]/div[@class=&quot;scroll-container&quot;]/pane-container[@class=&quot;ng-star-inserted&quot;]/div[@class=&quot;is-expanded&quot;]/div[@class=&quot;relative left item-container ng-star-inserted&quot;]/div[@class=&quot;left mleft20 main-info-container&quot;]/div[@class=&quot;w70 left row&quot;]/div[@class=&quot;left ellipsis col single-page&quot;]/mdl-select[@class=&quot;mdl-select has-placeholder ng-untouched ng-valid ng-dirty&quot;]/div[@class=&quot;mdl-textfield is-upgraded is-focused&quot;]/span[@class=&quot;mdl-select__toggle material-icons&quot;]</value>
      <webElementGuid>9b0d1bff-3caf-43c1-bafc-39b6bd7dad23</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Edition Names'])[1]/following::span[2]</value>
      <webElementGuid>b2052a50-7c96-4820-b160-c2f91221efe6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Supports Revisions'])[1]/following::span[2]</value>
      <webElementGuid>1609e776-fc1d-4365-bc88-99f59dd15083</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='keyboard_arrow_down'])[2]/preceding::span[3]</value>
      <webElementGuid>3a864e6e-a5df-4cdd-a587-6ba7c0e92669</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='keyboard_arrow_down'])[3]/preceding::span[6]</value>
      <webElementGuid>4f6147f8-f56a-4230-9a19-19d5ca0e0948</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='keyboard_arrow_down']/parent::*</value>
      <webElementGuid>707cc981-a0c7-43f7-a1b1-5e5dedf567dd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//span[2]</value>
      <webElementGuid>f175f624-75ff-484b-a269-f36c1219cd1c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//span[(text() = ' keyboard_arrow_down ' or . = ' keyboard_arrow_down ')]</value>
      <webElementGuid>3b00d42f-2e1f-43c1-8bca-585e282313c3</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
